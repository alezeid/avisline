import './App.css';


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>ООО АВИСЛАЙН ДИСТРИБЬЮШН</h1>   
        <h2>Строительный крепеж и профили из тонколистового металла</h2>
        <div><a href="tel:+73437772620">+7 34377 7-26-20</a></div>
        <div className="adress">
          ИНН 6683011937 КПП 668301001
          ОГРН 1169658136449 ОКПО 05784756
          Юридический адрес: 624250, Свердловская область,
          г. Заречный, ул. Промзона БЗСК, Строение 2
        <div><a href="mailto:av_distrib@mail.ru">написать нам</a></div>        
        </div>     
      </header>       
      <div className='container'>              
        <div className="kp"><a href="https://disk.yandex.ru/i/6bWYkQMgK7SUBg" download>Коммерческое предложение</a></div>
        <div className="about"><a href="https://disk.yandex.ru/i/nfvTYQEWzbyZdQ" download>Карточка предприятия</a></div>    
      </div>
    </div>
  );
}

export default App;
